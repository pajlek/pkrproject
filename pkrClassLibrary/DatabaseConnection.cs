﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace pkrClassLibrary
{
    public class DatabaseConnection
    {
        private string sql_string;  //sql query string
        private string strCon;      //connection string 
        SqlDataAdapter da_1; //adapter

        public string Sql //set sql query jer je private
        {
            set { sql_string = value; }
        }

        public string connection_string  //set connection string jer je private
        {
            set { strCon = value; }
        }

        public DataSet GetConnection
        {
            get { return MyDataSet(); }
        }

        private DataSet MyDataSet() //vraca ispunjenji DataSet
        {
            //dajemo connection string za connect na bazu
            SqlConnection con = new SqlConnection( strCon );

            con.Open(); //otvaranje connection

            da_1 = new SqlDataAdapter(sql_string, con); //inicjalizacija adaptera i predaja sql_stringa

            DataSet dat_set = new DataSet(); //novi DataSet
            da_1.Fill(dat_set, "Table_Data_1"); //ispunit DataSet preko DataAdaptera
            con.Close(); //zatvaranje connection

            return dat_set; //vrati ispunjenji DataSet
        }

        public void UpdateDatabase (System.Data.DataSet ds)
        {
            //koristimo SqlCommandBuilder za update itd.
            SqlCommandBuilder cb = new SqlCommandBuilder(da_1);
            cb.DataAdapter.Update(ds.Tables[0]); //cb ima property DataAdapter preko kojeg se koristi funkcija Update/insert? koja salje bilo kakav update na bazu
        }
    }
}
