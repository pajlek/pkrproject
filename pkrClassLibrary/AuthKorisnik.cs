﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pkrClassLibrary
{
    public class AuthKorisnik
    {
        public static string UlogiraniKorisnik { get; set; }
        public static int RazinaPrava { get; set; }   //0 admin, 1 korisnik (samo kasa)
    }
}
