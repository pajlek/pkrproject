# README #

**pkrKasa** is a project ment for c# / Forms / MSSQL / VisualStudio / git learning.

It's a POS program with following forms added: 
Login (Admin [000], and Regular User [111])
Products (CRUD),
Users (CRUD),
POS form (receipt entry)

All info from recipts/products/users are saved on a MS SQL server.

MS SQL tables:
Products,
ReceiptHeader,
ReceiptItem,
Users

Screenshot of KasaForm: ![0773407e891ba1853807bcae3b17bf45.png](https://bitbucket.org/repo/zaraKq/images/758452895-0773407e891ba1853807bcae3b17bf45.png)