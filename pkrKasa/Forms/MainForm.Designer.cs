﻿namespace pkrKasa
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.pkrMenu = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiKasa = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProizvodi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiKorisnici = new System.Windows.Forms.ToolStripMenuItem();
            this.lblUlogirani = new System.Windows.Forms.Label();
            this.pkrMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(72, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(375, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "pkrKasa Solution Demo";
            // 
            // pkrMenu
            // 
            this.pkrMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.pkrMenu.Location = new System.Drawing.Point(0, 0);
            this.pkrMenu.Name = "pkrMenu";
            this.pkrMenu.Size = new System.Drawing.Size(516, 24);
            this.pkrMenu.TabIndex = 1;
            this.pkrMenu.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiKasa,
            this.tsmiProizvodi,
            this.tsmiKorisnici});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // tsmiKasa
            // 
            this.tsmiKasa.Name = "tsmiKasa";
            this.tsmiKasa.Size = new System.Drawing.Size(152, 22);
            this.tsmiKasa.Text = "Kasa";
            this.tsmiKasa.Click += new System.EventHandler(this.kasaTestingToolStripMenuItem_Click);
            // 
            // tsmiProizvodi
            // 
            this.tsmiProizvodi.Name = "tsmiProizvodi";
            this.tsmiProizvodi.Size = new System.Drawing.Size(152, 22);
            this.tsmiProizvodi.Text = "Proizvodi";
            this.tsmiProizvodi.Click += new System.EventHandler(this.proizvodiToolStripMenuItem_Click);
            // 
            // tsmiKorisnici
            // 
            this.tsmiKorisnici.Name = "tsmiKorisnici";
            this.tsmiKorisnici.Size = new System.Drawing.Size(152, 22);
            this.tsmiKorisnici.Text = "Korisnici";
            this.tsmiKorisnici.Click += new System.EventHandler(this.korisniciToolStripMenuItem_Click);
            // 
            // lblUlogirani
            // 
            this.lblUlogirani.AutoSize = true;
            this.lblUlogirani.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUlogirani.Location = new System.Drawing.Point(219, 268);
            this.lblUlogirani.Name = "lblUlogirani";
            this.lblUlogirani.Size = new System.Drawing.Size(70, 22);
            this.lblUlogirani.TabIndex = 2;
            this.lblUlogirani.Text = "ulogirani";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 299);
            this.Controls.Add(this.lblUlogirani);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pkrMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.pkrMenu;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "pkrKasa Demo";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.pkrMenu.ResumeLayout(false);
            this.pkrMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip pkrMenu;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiProizvodi;
        private System.Windows.Forms.ToolStripMenuItem tsmiKasa;
        private System.Windows.Forms.Label lblUlogirani;
        private System.Windows.Forms.ToolStripMenuItem tsmiKorisnici;
    }
}

