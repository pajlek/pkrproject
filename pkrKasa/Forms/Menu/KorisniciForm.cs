﻿using System;
using System.Data;
using System.Windows.Forms;
using pkrClassLibrary;

namespace pkrKasa
{
    public partial class KorisniciForm : Form
    {
       
        public KorisniciForm()
        {
            InitializeComponent();
        }

        DatabaseConnection objConnect;  
        string conString;
        bool editLocked = true;

        DataSet ds;     //negdje moramo spemit DataSet iz nase DatabaseConnection class (objConnect)
        DataRow dRow;   //da bi mogli manipulirati svakim redom

        int MaxRows;    //koliko retka je u DataSetu, tj koliko retka smo izvukli iz tablice
        int incrementRow = 0;    //na koji retak u DataRow pokazuje tj. u DataSetu 

        private void ProizvodiForm_Load(object sender, EventArgs e)
        {
            try
            {
                objConnect = new DatabaseConnection(); //inicjalizacija objekta objConnect
                conString = Properties.Settings.Default.pkrDataBaseConnectionString; //imamo spremljen string u properties.settings.pkrData...

                objConnect.connection_string = conString; //predajemo conString u property set u DatabaseConnection classu
                objConnect.Sql = "SELECT * FROM Korisnici";  //predajemo sql nasoj classi

                ds = objConnect.GetConnection; //poziva MyDataSet i ispunjuje nas ds u ovoj formi

                MaxRows = ds.Tables[0].Rows.Count; //spremamo koliko retka ima u nasem ds i to u prvoj i jedinoj tablici u datasetu ( 0 index jer je prvi)

                NavigateRecords(); //moramo pozvat funkciju koju smo kreirali
                
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

        }

        private void NavigateRecords()  //ispunjavanje forme ProizvodiForm
        {
            editLocked = true;
            //dRow pokazuje na prvi retak u tablici Proizvod incrementRow je napocetku 0
            dRow = ds.Tables[0].Rows[incrementRow]; //dohvaca prvi retak iz prve tablice u DataSetu i sprema u dRow koji je tip DataRow

            //ItemArray u dRow, pokazuje na vrijednost u odredjenom retku
            txtSifra.Text = dRow.ItemArray.GetValue(1).ToString(); //row[0],ItemArray(1 stupac, jer je 0->id, 1->sifra)
            txtIme.Text = dRow.ItemArray.GetValue(2).ToString();
            txtRazinaPrava.Text = dRow.ItemArray.GetValue(4).ToString();
            

            TextBoxReadOnly(true); //postavlja textbox na read only
            KorisniciFooter();
        }

        private void TextBoxReadOnly(bool args)
        {
            if (!args)
            {
                txtRazinaPrava.ReadOnly = false;
                txtIme.ReadOnly = false;
                txtSifra.ReadOnly = false;
            }
            else
            {
                txtRazinaPrava.ReadOnly = true;
                txtIme.ReadOnly = true;
                txtSifra.ReadOnly = true;
            }
        }

        private void KorisniciFooter()
        {
            uint i = (uint)incrementRow + 1;
            txtFooter.Text = "Korisnik " + i + " od " + MaxRows;
        }

        private void btnNext_Click(object sender, EventArgs e)  //next row
        {
            if(incrementRow != MaxRows - 1) //ako je pokazivac na retak nije veci od broj maxrowa
            {
                incrementRow++;         //povecaj pokazivac za jedan
                NavigateRecords();      //ponovo ucitaj formu za novi redak
            }
            else
            {
                MessageBox.Show("Nema više korisnika poslje!");
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)  //prev row
        {
            if (incrementRow > 0)
            {
                incrementRow--;
                NavigateRecords();
            }
            else
            {
                MessageBox.Show("Nema više korisnika prije!");
            }
        }

        private void btnLast_Click(object sender, EventArgs e)  //last row
        {
            if (incrementRow != MaxRows - 1)
            {
                incrementRow = MaxRows - 1;
                NavigateRecords();
            }
            else
            {
                MessageBox.Show("Nema više korisnika poslje!");
            }
        }

        private void btnFirst_Click(object sender, EventArgs e) //first row
        {
            if (incrementRow > 0)
            {
                incrementRow = 0;
                NavigateRecords();
            }
            else
            {
                MessageBox.Show("Nema više proizvoda prije!");
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            TextBoxReadOnly(false);

            txtSifra.Clear();
            txtIme.Clear();
            txtRazinaPrava.Clear();

            btnNext.Enabled = false;
            btnPrevious.Enabled = false;
            btnFirst.Enabled = false;
            btnLast.Enabled = false;
            btnDelete.Enabled = false;
            btnEdit.Enabled = false;

            btnAddNew.Enabled = false;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigateRecords();

            btnNext.Enabled = true;
            btnPrevious.Enabled = true;
            btnFirst.Enabled = true;
            btnLast.Enabled = true;
            btnDelete.Enabled = true;
            btnEdit.Enabled = true;

            btnAddNew.Enabled = true;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (editLocked) //nije u edit stanju (tj. novi redak)
            {
                DataRow row = ds.Tables[0].NewRow(); //novi retak u datasetu preko datarow objecta
                row[1] = txtSifra.Text;     //punjenja DataRow sa podacima iz txtBoxa
                row[2] = txtIme.Text;
                row[4] = txtRazinaPrava.Text;

                ds.Tables[0].Rows.Add(row); //dodaj row u DataSet

                try
                {
                    objConnect.UpdateDatabase(ds); //koristimo nasu metodu u classi DbConnection i predajemo dataset ds

                    MaxRows += 1; //moramo povecat maxrows za jedan
                    incrementRow = MaxRows - 1; //increment je uvijek jedan manji od maxRows;

                    MessageBox.Show("Novi korisnik je dodan!");
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
            else            //edit tj. update rowa
            {
                DataRow row = ds.Tables[0].Rows[incrementRow]; //postavi pokazivac na trenutni red
                row[1] = txtSifra.Text;     //punjenja DataRow sa podacima iz txtBoxa
                row[2] = txtIme.Text;
                row[4] = txtRazinaPrava.Text;

                try
                {
                    objConnect.UpdateDatabase(ds); //koristimo nasu metodu u classi DbConnection i predajemo dataset ds

                    MessageBox.Show("Korisnik: '" + txtIme.Text + "' je editiran.");
                    editLocked = true;
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }

            btnNext.Enabled = true;
            btnPrevious.Enabled = true;
            btnFirst.Enabled = true;
            btnLast.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;

            btnCancel.Enabled = false;
            btnSave.Enabled = false;
            btnAddNew.Enabled = true;

            NavigateRecords();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            editLocked = false;

            TextBoxReadOnly(false);
            btnDelete.Enabled = false;
            btnAddNew.Enabled = false;
            btnCancel.Enabled = true;
            btnSave.Enabled = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                ds.Tables[0].Rows[incrementRow].Delete();   //brisemo onaj row na koji incrementRow pokazuje
                objConnect.UpdateDatabase(ds);  //pozivamo metodu updatedatabase koja prima ds i to se salje preko adapter na stvarnu DB

                MaxRows = ds.Tables[0].Rows.Count;  //moramo postavit novi rowcount jer se smanjio za jedan
                incrementRow--;
                NavigateRecords();

                MessageBox.Show("Korisnik je obrisan!");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
           Close();
           new MainForm().Show();
        }
    }
}
