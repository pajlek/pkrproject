﻿namespace pkrKasa
{
    partial class ProizvodiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProizvodiForm));
            this.txtProizvodSifra = new System.Windows.Forms.TextBox();
            this.txtProizvodNaziv = new System.Windows.Forms.TextBox();
            this.txtProizvodOpis = new System.Windows.Forms.TextBox();
            this.txtProizvodCijena = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNaslovProizvodi = new System.Windows.Forms.Label();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.txtFooter = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtFindProizvod = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtProizvodSifra
            // 
            this.txtProizvodSifra.Location = new System.Drawing.Point(174, 124);
            this.txtProizvodSifra.Name = "txtProizvodSifra";
            this.txtProizvodSifra.Size = new System.Drawing.Size(137, 20);
            this.txtProizvodSifra.TabIndex = 0;
            // 
            // txtProizvodNaziv
            // 
            this.txtProizvodNaziv.Location = new System.Drawing.Point(174, 170);
            this.txtProizvodNaziv.Name = "txtProizvodNaziv";
            this.txtProizvodNaziv.Size = new System.Drawing.Size(138, 20);
            this.txtProizvodNaziv.TabIndex = 1;
            // 
            // txtProizvodOpis
            // 
            this.txtProizvodOpis.Location = new System.Drawing.Point(174, 267);
            this.txtProizvodOpis.Name = "txtProizvodOpis";
            this.txtProizvodOpis.Size = new System.Drawing.Size(138, 20);
            this.txtProizvodOpis.TabIndex = 3;
            // 
            // txtProizvodCijena
            // 
            this.txtProizvodCijena.Location = new System.Drawing.Point(174, 218);
            this.txtProizvodCijena.Name = "txtProizvodCijena";
            this.txtProizvodCijena.Size = new System.Drawing.Size(137, 20);
            this.txtProizvodCijena.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(137, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Šifra:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(130, 173);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Naziv:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cijena:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(138, 270);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Opis:";
            // 
            // lblNaslovProizvodi
            // 
            this.lblNaslovProizvodi.AutoSize = true;
            this.lblNaslovProizvodi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaslovProizvodi.Location = new System.Drawing.Point(12, 9);
            this.lblNaslovProizvodi.Name = "lblNaslovProizvodi";
            this.lblNaslovProizvodi.Size = new System.Drawing.Size(186, 25);
            this.lblNaslovProizvodi.TabIndex = 8;
            this.lblNaslovProizvodi.Text = "Pregled proizvoda";
            // 
            // btnPrevious
            // 
            this.btnPrevious.Image = global::pkrKasa.Properties.Resources.left;
            this.btnPrevious.Location = new System.Drawing.Point(71, 19);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(50, 23);
            this.btnPrevious.TabIndex = 10;
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Image = global::pkrKasa.Properties.Resources.right;
            this.btnNext.Location = new System.Drawing.Point(127, 19);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(50, 23);
            this.btnNext.TabIndex = 11;
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnLast
            // 
            this.btnLast.Image = global::pkrKasa.Properties.Resources.far_right;
            this.btnLast.Location = new System.Drawing.Point(183, 19);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(50, 23);
            this.btnLast.TabIndex = 12;
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAddNew);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Location = new System.Drawing.Point(53, 312);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 88);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Novi proizvod";
            // 
            // btnAddNew
            // 
            this.btnAddNew.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAddNew.Image = global::pkrKasa.Properties.Resources.add;
            this.btnAddNew.Location = new System.Drawing.Point(40, 35);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 25);
            this.btnAddNew.TabIndex = 13;
            this.btnAddNew.Text = "Novi";
            this.btnAddNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Enabled = false;
            this.btnCancel.Image = global::pkrKasa.Properties.Resources.odustani;
            this.btnCancel.Location = new System.Drawing.Point(135, 35);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Odustani";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Image = global::pkrKasa.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(227, 35);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 25);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Spremi";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnEdit);
            this.groupBox2.Controls.Add(this.btnPrevious);
            this.groupBox2.Controls.Add(this.btnFirst);
            this.groupBox2.Controls.Add(this.btnLast);
            this.groupBox2.Controls.Add(this.btnNext);
            this.groupBox2.Location = new System.Drawing.Point(12, 46);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(421, 61);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kontrola";
            // 
            // btnDelete
            // 
            this.btnDelete.Image = global::pkrKasa.Properties.Resources.delete;
            this.btnDelete.Location = new System.Drawing.Point(333, 19);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 23);
            this.btnDelete.TabIndex = 18;
            this.btnDelete.Text = "Obriši";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(257, 19);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(70, 23);
            this.btnEdit.TabIndex = 18;
            this.btnEdit.Text = "Izmjeni";
            this.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.Image = global::pkrKasa.Properties.Resources.far_left;
            this.btnFirst.Location = new System.Drawing.Point(15, 19);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(50, 23);
            this.btnFirst.TabIndex = 9;
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // txtFooter
            // 
            this.txtFooter.AutoSize = true;
            this.txtFooter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtFooter.Location = new System.Drawing.Point(174, 417);
            this.txtFooter.Name = "txtFooter";
            this.txtFooter.Size = new System.Drawing.Size(37, 15);
            this.txtFooter.TabIndex = 18;
            this.txtFooter.Text = "label5";
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(369, 9);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 19;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtFindProizvod
            // 
            this.txtFindProizvod.Location = new System.Drawing.Point(228, 14);
            this.txtFindProizvod.Name = "txtFindProizvod";
            this.txtFindProizvod.Size = new System.Drawing.Size(84, 20);
            this.txtFindProizvod.TabIndex = 0;
            this.txtFindProizvod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFindProizvod.TextChanged += new System.EventHandler(this.txtFindProizvod_TextChanged);
            this.txtFindProizvod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFindProizvod_KeyPress);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::pkrKasa.Properties.Resources.zoom;
            this.pictureBox1.Location = new System.Drawing.Point(318, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // ProizvodiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 445);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtFindProizvod);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.txtFooter);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblNaslovProizvodi);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtProizvodCijena);
            this.Controls.Add(this.txtProizvodOpis);
            this.Controls.Add(this.txtProizvodNaziv);
            this.Controls.Add(this.txtProizvodSifra);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProizvodiForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ProizvodiForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtProizvodSifra;
        private System.Windows.Forms.TextBox txtProizvodNaziv;
        private System.Windows.Forms.TextBox txtProizvodOpis;
        private System.Windows.Forms.TextBox txtProizvodCijena;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblNaslovProizvodi;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label txtFooter;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtFindProizvod;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}