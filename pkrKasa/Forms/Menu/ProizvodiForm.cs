﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using pkrClassLibrary;

namespace pkrKasa
{
    public partial class ProizvodiForm : Form
    {
       
        public ProizvodiForm()
        {
            InitializeComponent();
        }

        DatabaseConnection objConnect;  
        string conString;
        bool editLocked = true;

        DataSet ds;     //negdje moramo spemit DataSet iz nase DatabaseConnection class (objConnect)

        int MaxRows;    //koliko retka je u DataSetu, tj koliko retka smo izvukli iz tablice
        int incrementRow = 0;    //na koji retak u DataRow pokazuje tj. u DataSetu 

        CurrencyManager cm;
        DataRow[] foundRow;

        private void ProizvodiForm_Load(object sender, EventArgs e)
        {
            try
            {
                objConnect = new DatabaseConnection(); //inicjalizacija objekta objConnect
                conString = Properties.Settings.Default.pkrDataBaseConnectionString; 

                objConnect.connection_string = conString; //predajemo conString u property set u DatabaseConnection classu
                objConnect.Sql = "SELECT * FROM Proizvodi";  //predajemo sql nasoj classi

                ds = objConnect.GetConnection; //poziva MyDataSet i ispunjuje nas ds u ovoj formi

                MaxRows = ds.Tables[0].Rows.Count; //spremamo koliko retka ima u nasem ds i to u prvoj i jedinoj tablici u datasetu ( 0 index jer je prvi)

                //-------------------
                txtProizvodSifra.DataBindings.Add("Text", ds.Tables[0], "sifra");
                txtProizvodNaziv.DataBindings.Add("Text", ds.Tables[0], "naziv");
                txtProizvodCijena.DataBindings.Add("Text", ds.Tables[0], "cijena");
                txtProizvodOpis.DataBindings.Add("Text", ds.Tables[0], "opis");

                cm = (CurrencyManager)BindingContext[ds.Tables[0]];
                long rowPosition = cm.Position;

                NavigateRecords(); 
                
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

        }

        private void NavigateRecords()  //ispunjavanje forme ProizvodiForm
        {
            editLocked = true;
            TextBoxReadOnly(true); //postavlja textbox na read only
            UpdateFooter();
        }

        private void TextBoxReadOnly(bool a)
        {
            if (!a)
            {
                txtProizvodCijena.ReadOnly = false;
                txtProizvodNaziv.ReadOnly = false;
                txtProizvodOpis.ReadOnly = false;
                txtProizvodSifra.ReadOnly = false;
            }
            else
            {
                txtProizvodCijena.ReadOnly = true;
                txtProizvodNaziv.ReadOnly = true;
                txtProizvodOpis.ReadOnly = true;
                txtProizvodSifra.ReadOnly = true;
            }
        }

        private void UpdateFooter()
        {
            uint i = (uint)cm.Position + 1;
            txtFooter.Text = "Proizvod " + i + " od " + cm.Count;
        }

        private void btnNext_Click(object sender, EventArgs e)  //next row
        {
            if(cm.Position != cm.Count - 1) //ako je pokazivac na retak nije veci od broj maxrowa
            {
                cm.Position++;
                NavigateRecords();      //ponovo ucitaj formu za novi redak
            }
            else
            {
                MessageBox.Show("Nema više proizvoda poslje!");
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)  //prev row
        {
            if (cm.Position > 0)
            {
                cm.Position--;
                NavigateRecords();
            }
            else
            {
                MessageBox.Show("Nema više proizvoda prije!");
            }
        }

        private void btnLast_Click(object sender, EventArgs e)  //last row
        {
            if (cm.Position != cm.Count - 1)
            {
                cm.Position = cm.Count - 1;
                NavigateRecords();
            }
            else
            {
                MessageBox.Show("Nema više proizvoda poslje!");
            }
        }

        private void btnFirst_Click(object sender, EventArgs e) //first row
        {
            if (cm.Position > 0)
            {
                cm.Position = 0;
                NavigateRecords();
            }
            else
            {
                MessageBox.Show("Nema više proizvoda prije!");
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            TextBoxReadOnly(false);

            txtProizvodSifra.Clear();
            txtProizvodNaziv.Clear();
            txtProizvodCijena.Clear();
            txtProizvodOpis.Clear();

            btnNext.Enabled = false;
            btnPrevious.Enabled = false;
            btnFirst.Enabled = false;
            btnLast.Enabled = false;
            btnDelete.Enabled = false;
            btnEdit.Enabled = false;

            btnAddNew.Enabled = false;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            NavigateRecords();

            btnNext.Enabled = true;
            btnPrevious.Enabled = true;
            btnFirst.Enabled = true;
            btnLast.Enabled = true;
            btnDelete.Enabled = true;
            btnEdit.Enabled = true;

            btnAddNew.Enabled = true;
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (editLocked) //nije u edit stanju (tj. novi redak)
            {
                DataRow row = ds.Tables[0].NewRow(); //novi retak u datasetu preko datarow objecta
                row[1] = txtProizvodSifra.Text;     //punjenja DataRow sa podacima iz txtBoxa
                row[2] = txtProizvodNaziv.Text;
                row[3] = txtProizvodCijena.Text;
                row[4] = txtProizvodOpis.Text;

                ds.Tables[0].Rows.Add(row); //dodaj row u DataSet

                try
                {
                    objConnect.UpdateDatabase(ds); //koristimo nasu metodu u classi DbConnection i predajemo dataset ds
                    MessageBox.Show("Database updated!");
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
            else            //edit tj. update rowa
            {
                DataRow row = ds.Tables[0].Rows[incrementRow]; //postavi pokazivac na trenutni red
                row[1] = txtProizvodSifra.Text;     //punjenja DataRow sa podacima iz txtBoxa
                row[2] = txtProizvodNaziv.Text;
                row[3] = txtProizvodCijena.Text;
                row[4] = txtProizvodOpis.Text;

                try
                {
                    objConnect.UpdateDatabase(ds); //koristimo nasu metodu u classi DbConnection i predajemo dataset ds

                    MessageBox.Show("Proizvod: '" + txtProizvodNaziv.Text + "' je editiran.");
                    editLocked = true;
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }

            btnNext.Enabled = true;
            btnPrevious.Enabled = true;
            btnFirst.Enabled = true;
            btnLast.Enabled = true;
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;

            btnCancel.Enabled = false;
            btnSave.Enabled = false;
            btnAddNew.Enabled = true;

            NavigateRecords();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            editLocked = false;

            TextBoxReadOnly(false);
            btnDelete.Enabled = false;
            btnAddNew.Enabled = false;
            btnCancel.Enabled = true;
            btnSave.Enabled = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                ds.Tables[0].Rows[incrementRow].Delete();   //brisemo onaj row na koji incrementRow pokazuje
                objConnect.UpdateDatabase(ds);  //pozivamo metodu updatedatabase koja prima ds i to se salje preko adapter na stvarnu DB

                NavigateRecords();

                MessageBox.Show("Proizvod je obrisan!");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
           Close();
           new MainForm().Show();
        }

        private void txtFindProizvod_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //ako ima texta
                if (txtFindProizvod.Text.Length != 0)
                {
                    foundRow = ds.Tables[0].Select("sifra LIKE '" + txtFindProizvod.Text + "%'");
                    if (foundRow.Count() != 0)
                    {
                        cm.Position = ds.Tables[0].Rows.IndexOf(foundRow[0]);
                        UpdateFooter();
                    }
                    else
                    {
                        txtFindProizvod.BackColor = Color.Red;
                    }
                }//nema texta
                else
                {
                    txtFindProizvod.BackColor = Color.White;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void txtFindProizvod_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                if (!string.IsNullOrWhiteSpace(txtFindProizvod.Text))
                {
                    txtFindProizvod.Text = null;
                }
                else
                {
                    Close();
                    new MainForm().Show();
                }
            }
        }
    }
}
