﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using pkrClassLibrary;
using System.Data.SqlClient;
using System.Diagnostics;

namespace pkrKasa.Forms
{
    public partial class KasaForm : Form
    {
        public KasaForm()
        {
            InitializeComponent();
        }

        SqlConnection conn;
        SqlDataAdapter adptP, adptZ, adptS, adptSU;
        DataSet ds;
        BindingSource bsP, bsS;
        SqlCommandBuilder cbZ, cbS;
        DataRow rowZ;
        int zadnjiRacun;
        int rowStavkeCount = -1;

        private void TestingKasaForm_Load(object sender, EventArgs e)
        {
            conn = new SqlConnection(Properties.Settings.Default.pkrDataBaseConnectionString);
            adptP = new SqlDataAdapter("SELECT * FROM Proizvodi", conn);
            adptZ = new SqlDataAdapter("SELECT * FROM RacuniZaglavlje", conn);
            adptS = new SqlDataAdapter("SELECT RacuniStavke.id, RacuniStavke.idZaglavlja, RacuniStavke.idProizvoda, RacuniStavke.cijena, RacuniStavke.kolicina, Proizvodi.sifra, Proizvodi.naziv FROM RacuniStavke INNER JOIN Proizvodi ON RacuniStavke.idProizvoda = Proizvodi.id", conn);
            adptSU = new SqlDataAdapter("SELECT * FROM RacuniStavke", conn);
            ds = new DataSet();
            bsP = new BindingSource();
            bsS = new BindingSource();

            try
            {
                adptP.Fill(ds, "Proizvodi");
                adptZ.Fill(ds, "RacuniZaglavlje");
                adptS.Fill(ds, "RacuniStavkeDGV");
                adptSU.Fill(ds, "RacuniStavke");

                //povezivanje dataseta i bindingsa za proizvodi datagridview
                bsP.DataSource = ds;
                bsP.DataMember = ds.Tables["Proizvodi"].TableName;
                dgvProizvodi.DataSource = bsP;

                //povezivanje dataseta i bindingsa za stavke datagridview
                bsS.DataSource = ds;
                bsS.DataMember = ds.Tables["RacuniStavkeDGV"].TableName;
                dgvStavke.DataSource = bsS;
                ds.Tables["RacuniStavkeDGV"].Columns.Add("stavkaUkupno");
                ds.Tables["RacuniStavkeDGV"].Columns["stavkaUkupno"].Expression = "kolicina*cijena";  //izracun stavkaUkupno

                //postavljanje parent-child relations (zaglavlje-stavka racuna)
                DataRelation relZagStavke;
                DataColumn idZag;
                DataColumn idStav;
                idZag = ds.Tables["RacuniZaglavlje"].Columns["id"];
                idStav = ds.Tables["RacuniStavkeDGV"].Columns["idZaglavlja"];
                relZagStavke = new DataRelation("RelZagStavke", idZag, idStav);
                ds.Relations.Add(relZagStavke);

                //novi racun. tj novi redak u RacuniZaglavlje i ispun novog retka sa podacima
                zadnjiRacun = ds.Tables["RacuniZaglavlje"].Rows.Count + 1;

                NovoZaglavlje();

                txtBrRacuna.Text = rowZ.ItemArray.GetValue(0).ToString();
                txtDatumRacuna.Text = rowZ.ItemArray.GetValue(1).ToString();
                txtUkupnoRacun.Text = rowZ.ItemArray.GetValue(2).ToString();

                //prikazi stavke samo za racun txtBrRacuna (prazno je je novi racun) 
                bsS.Filter = string.Format("idZaglavlja = '{0}'", txtBrRacuna.Text);

                //za update database preko dataseta (RacuniZaglavlje i RacuniStavke)
                cbZ = new SqlCommandBuilder(adptZ);
                cbS = new SqlCommandBuilder(adptSU);

                lblInfo.Text = "Spremno za unos.";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (rowStavkeCount > -1)
                { 
                    //RacuniZaglavlje update
                    rowZ[2] = txtUkupnoRacun.Text;  //postavi konacnu cijenu
                    cbZ.DataAdapter.Update(ds, "RacuniZaglavlje");  //posalji update preko adaptera

                    //RacuniStavke update
                    cbS.DataAdapter.Update(ds, "RacuniStavke");

                    lblInfo.Text = "Racun br. " + txtBrRacuna.Text + " je obrađen, spremno za novi unos.";

                    //postavljanje za novi upis
                    zadnjiRacun++;
                    rowStavkeCount = -1;
                    NovoZaglavlje();

                    txtBrRacuna.Text = rowZ.ItemArray.GetValue(0).ToString();
                    txtDatumRacuna.Text = rowZ.ItemArray.GetValue(1).ToString();
                    txtUkupnoRacun.Text = rowZ.ItemArray.GetValue(2).ToString();

                    bsS.Filter = string.Format("idZaglavlja = '{0}'", txtBrRacuna.Text);

                    txtKolicinaProizvoda.Text = null;
                    txtNazivProizvoda.Text = null;
                    txtCijenaProizvoda.Text = null;
                    txtFindProizvod.Text = null;
                    txtFindProizvod.Focus();
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
            }
        }

        private void NovoZaglavlje()
        {
            rowZ = ds.Tables["RacuniZaglavlje"].NewRow();
            rowZ[0] = zadnjiRacun;
            rowZ[1] = DateTime.Now;
            rowZ[2] = 0.0;
            ds.Tables["RacuniZaglavlje"].Rows.Add(rowZ);   //dodaj redak u dataset
        }

  
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (rowStavkeCount > -1)
            {
                //cells[2] = idZag, cells[7] = idProiz
                DataRow[] stavkaDgvDelete = ds.Tables["RacuniStavkeDGV"].Select("idZaglavlja = '" + dgvStavke.SelectedCells[2].Value.ToString() + "' and idProizvoda = '" + dgvStavke.SelectedCells[7].Value.ToString() + "'");
                DataRow[] stavkaDelete = ds.Tables["RacuniStavke"].Select("idZaglavlja = '" + dgvStavke.SelectedCells[2].Value.ToString() + "' and idProizvoda = '" + dgvStavke.SelectedCells[7].Value.ToString() + "'");

                //spremamo kolika je cijena stavke kako bi mogli smanjiti UkupnoRacun
                decimal obrisanaStavkaUkupno = Convert.ToDecimal(stavkaDgvDelete[0]["stavkaUkupno"]);
                txtUkupnoRacun.Text = (decimal.Parse(txtUkupnoRacun.Text) - obrisanaStavkaUkupno).ToString();

                lblInfo.Text = "Proizvod-sifra: " + dgvStavke.SelectedCells[1].Value.ToString() + " je obrisan.";

                int indexStavkeDGV = ds.Tables["RacuniStavkeDGV"].Rows.IndexOf(stavkaDgvDelete[0]);
                int indexStavke = ds.Tables["RacuniStavke"].Rows.IndexOf(stavkaDelete[0]);

                ds.Tables["RacuniStavkeDGV"].Rows.RemoveAt(indexStavkeDGV);
                ds.Tables["RacuniStavke"].Rows.RemoveAt(indexStavke);

                rowStavkeCount -= 1;
                if (rowStavkeCount > -1)
                {
                    dgvStavke.Rows[rowStavkeCount].Selected = true;
                }

                txtFindProizvod.Focus();
            }
        }

        private void txtFindProizvod_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //ako ima texta
                if (txtFindProizvod.Text.Length != 0)
                {
                    bsP.Filter = "sifra LIKE '" + txtFindProizvod.Text + "%'";
                    if (dgvProizvodi.Rows.Count == 0)
                    {
                        txtFindProizvod.BackColor = Color.Red;
                        lblInfo.Text = "Nema takvog proizvoda.";
                    }
                    else
                    {
                        txtFindProizvod.BackColor = Color.White;
                        lblInfo.Text = "Spremno za unos.";
                    }
                }//nema texta
                else
                {
                    txtFindProizvod.BackColor = Color.White;
                    bsP.RemoveFilter();
                    lblInfo.Text = "Spremno za unos.";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void txtKolicinaProizvoda_TextChanged(object sender, EventArgs e)
        {
            decimal kolicina;
            if (!decimal.TryParse(txtKolicinaProizvoda.Text, out kolicina) && !string.IsNullOrWhiteSpace(txtKolicinaProizvoda.Text))
            {
                txtKolicinaProizvoda.BackColor = Color.Red;
                lblInfo.Text = "Kolicina je nepravilna!";
            }
            else
            {
                txtKolicinaProizvoda.BackColor = Color.White;
                lblInfo.Text = "Spremno za unos.";
            }
        }

        private void txtFindProizvod_KeyPress(object sender, KeyPressEventArgs e)
        {   //escape -> brisanje texta
            if (e.KeyChar == (char)Keys.Escape)
            {
                if (!string.IsNullOrWhiteSpace(txtFindProizvod.Text))
                {
                    txtFindProizvod.Text = null;
                    txtFindProizvod.BackColor = Color.White;
                }
                else
                {
                    Close();
                    new MainForm().Show();
                }
            }

            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Return)
            {
                if (dgvProizvodi.Rows.Count != 0)
                {
                    txtFindProizvod.Text = dgvProizvodi.SelectedCells[1].Value.ToString();
                    txtNazivProizvoda.Text = dgvProizvodi.SelectedCells[2].Value.ToString();
                    txtCijenaProizvoda.Text = dgvProizvodi.SelectedCells[3].Value.ToString();
                    txtKolicinaProizvoda.Text = "1";
                    txtKolicinaProizvoda.Focus();
                    e.Handled = true;   //mute ding sound
                    txtKolicinaProizvoda.SelectionStart = 0;
                    txtKolicinaProizvoda.SelectionLength = txtKolicinaProizvoda.Text.Length;
                }
            }
        }

        private void txtKolicinaProizvoda_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == (char)Keys.Escape)
            {
                if (!string.IsNullOrWhiteSpace(txtKolicinaProizvoda.Text))
                {
                    txtKolicinaProizvoda.Text = null;
                }
                else
                {
                    txtKolicinaProizvoda.Text = null;
                    txtNazivProizvoda.Text = null;
                    txtCijenaProizvoda.Text = null;
                    txtFindProizvod.Focus();
                    txtFindProizvod.SelectionStart = 0;
                    txtFindProizvod.SelectionLength = txtFindProizvod.Text.Length;
                }
            }

            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Return)
            {
                decimal kolicina;
                e.Handled = true;

                if (string.IsNullOrWhiteSpace(txtFindProizvod.Text) || txtFindProizvod.BackColor == Color.Red)
                {
                    txtFindProizvod.Focus();
                }
                else
                {
                    //provjera da je kolicina unesena i da je kolicina pravilna
                    if (!string.IsNullOrWhiteSpace(txtKolicinaProizvoda.Text) && Decimal.TryParse(txtKolicinaProizvoda.Text, out kolicina))
                    {
                        //provjera dali postoji vec takva stavka, ako da edit retka i povecavanje kolicine stavke za txtKolicina
                        if (dgvStavke.Rows.Count != 0)
                        {
                            DataRow[] stavkaPostoji = ds.Tables["RacuniStavkeDGV"].Select("sifra = '" + txtFindProizvod.Text + "' and idZaglavlja = '" + txtBrRacuna.Text + "'");

                            if (stavkaPostoji.Length !=0)
                            {
                                kolicina = Convert.ToDecimal(stavkaPostoji[0]["kolicina"])
                                                 + Convert.ToDecimal(txtKolicinaProizvoda.Text);
                                stavkaPostoji[0]["kolicina"] = kolicina.ToString();

                                UpdateZaglavljeCijene();
                                FocusSifraProizvoda();
                            }
                            //nema iste stavke, tj. dodavanje nove
                            else
                            {
                                NovaStavka();
                                FocusSifraProizvoda();
                            }
                        }
                        //nema jos stavki, tj. dodavanje nove
                        else
                        {
                            NovaStavka();
                            FocusSifraProizvoda();
                        }
                    }
                    else
                    {
                        lblInfo.Text = "Kolicina je nepravilna ili niste unjeli sifru proizvoda!";
                    }
                }
            }
        }

        private void FocusSifraProizvoda()
        {
            txtFindProizvod.Text = null;
            txtKolicinaProizvoda.Text = null;
            txtNazivProizvoda.Text = null;
            txtCijenaProizvoda.Text = null;
            txtFindProizvod.Focus();
        }

        private void NovaStavka()
        {
            //punimo RacuniStavkeDGV sa novim retkom
            DataRow rowDGV = ds.Tables["RacuniStavkeDGV"].NewRow();
            rowDGV[1] = txtBrRacuna.Text;
            rowDGV[2] = dgvProizvodi.SelectedCells[0].Value.ToString();
            rowDGV[3] = Convert.ToDecimal(txtCijenaProizvoda.Text);
            rowDGV[4] = Convert.ToDecimal(txtKolicinaProizvoda.Text);
            rowDGV[5] = dgvProizvodi.SelectedCells[1].Value.ToString(); //sifra proizvoda
            rowDGV[6] = txtNazivProizvoda.Text;
            ds.Tables["RacuniStavkeDGV"].Rows.Add(rowDGV);    //dodaj redak u dataset

            //punimo RacuniStavke sa novim retkom
            DataRow rowStavke = ds.Tables["RacuniStavke"].NewRow();
            rowStavke[1] = txtBrRacuna.Text;
            rowStavke[2] = dgvProizvodi.SelectedCells[0].Value.ToString();
            rowStavke[3] = Convert.ToDecimal(txtCijenaProizvoda.Text);
            rowStavke[4] = Convert.ToDecimal(txtKolicinaProizvoda.Text);
            ds.Tables["RacuniStavke"].Rows.Add(rowStavke);

            rowStavkeCount = dgvStavke.Rows.Count - 1;
            if (rowStavkeCount > -1)  //select zadnjeg rowa nakon dodavanja
            {
                dgvStavke.Rows[rowStavkeCount].Selected = true;
            }

            UpdateZaglavljeCijene();
        }

        private void UpdateZaglavljeCijene()
        {
            //update konacne cijene za RacuniZaglavlje
            decimal cijena, kolicina, ukupno;
            if (!decimal.TryParse(txtCijenaProizvoda.Text, out cijena) ||
                !decimal.TryParse(txtKolicinaProizvoda.Text, out kolicina) ||
                !decimal.TryParse(txtUkupnoRacun.Text, out ukupno))
            {
                MessageBox.Show("nesto ne radi sa UpdateZaglavljeCijene");
            }
            else
            {
                txtUkupnoRacun.Text = (ukupno + (kolicina * cijena)).ToString();
            }
        }
    }
}
