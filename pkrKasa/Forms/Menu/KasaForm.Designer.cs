﻿namespace pkrKasa.Forms
{
    partial class KasaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvProizvodi = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sifraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazivDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cijenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proizvodiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pkrDataBaseDataSet = new pkrKasa.pkrDataBaseDataSet();
            this.txtFindProizvod = new System.Windows.Forms.TextBox();
            this.proizvodiTableAdapter = new pkrKasa.pkrDataBaseDataSetTableAdapters.ProizvodiTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCijenaProizvoda = new System.Windows.Forms.TextBox();
            this.txtKolicinaProizvoda = new System.Windows.Forms.TextBox();
            this.txtNazivProizvoda = new System.Windows.Forms.TextBox();
            this.txtBrRacuna = new System.Windows.Forms.TextBox();
            this.txtDatumRacuna = new System.Windows.Forms.TextBox();
            this.txtUkupnoRacun = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.fKRacuniStavkeProizvodiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.racuniStavkeTableAdapter = new pkrKasa.pkrDataBaseDataSetTableAdapters.RacuniStavkeTableAdapter();
            this.dgvStavke = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sifraDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idZaglavljaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazivDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cijenaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolicinaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stavkaUkupno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idProizvoda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProizvodi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proizvodiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pkrDataBaseDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fKRacuniStavkeProizvodiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStavke)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvProizvodi
            // 
            this.dgvProizvodi.AllowUserToAddRows = false;
            this.dgvProizvodi.AllowUserToDeleteRows = false;
            this.dgvProizvodi.AutoGenerateColumns = false;
            this.dgvProizvodi.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvProizvodi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProizvodi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.sifraDataGridViewTextBoxColumn,
            this.nazivDataGridViewTextBoxColumn,
            this.cijenaDataGridViewTextBoxColumn});
            this.dgvProizvodi.DataSource = this.proizvodiBindingSource;
            this.dgvProizvodi.Location = new System.Drawing.Point(21, 63);
            this.dgvProizvodi.Name = "dgvProizvodi";
            this.dgvProizvodi.ReadOnly = true;
            this.dgvProizvodi.RowHeadersWidth = 25;
            this.dgvProizvodi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProizvodi.Size = new System.Drawing.Size(268, 459);
            this.dgvProizvodi.TabIndex = 0;
            this.dgvProizvodi.TabStop = false;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // sifraDataGridViewTextBoxColumn
            // 
            this.sifraDataGridViewTextBoxColumn.DataPropertyName = "sifra";
            this.sifraDataGridViewTextBoxColumn.HeaderText = "sifra";
            this.sifraDataGridViewTextBoxColumn.Name = "sifraDataGridViewTextBoxColumn";
            this.sifraDataGridViewTextBoxColumn.ReadOnly = true;
            this.sifraDataGridViewTextBoxColumn.Width = 40;
            // 
            // nazivDataGridViewTextBoxColumn
            // 
            this.nazivDataGridViewTextBoxColumn.DataPropertyName = "naziv";
            this.nazivDataGridViewTextBoxColumn.HeaderText = "naziv";
            this.nazivDataGridViewTextBoxColumn.Name = "nazivDataGridViewTextBoxColumn";
            this.nazivDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cijenaDataGridViewTextBoxColumn
            // 
            this.cijenaDataGridViewTextBoxColumn.DataPropertyName = "cijena";
            this.cijenaDataGridViewTextBoxColumn.HeaderText = "cijena";
            this.cijenaDataGridViewTextBoxColumn.Name = "cijenaDataGridViewTextBoxColumn";
            this.cijenaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // proizvodiBindingSource
            // 
            this.proizvodiBindingSource.DataMember = "Proizvodi";
            this.proizvodiBindingSource.DataSource = this.pkrDataBaseDataSet;
            // 
            // pkrDataBaseDataSet
            // 
            this.pkrDataBaseDataSet.DataSetName = "pkrDataBaseDataSet";
            this.pkrDataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txtFindProizvod
            // 
            this.txtFindProizvod.Location = new System.Drawing.Point(6, 28);
            this.txtFindProizvod.Name = "txtFindProizvod";
            this.txtFindProizvod.Size = new System.Drawing.Size(108, 20);
            this.txtFindProizvod.TabIndex = 0;
            this.txtFindProizvod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFindProizvod.TextChanged += new System.EventHandler(this.txtFindProizvod_TextChanged);
            this.txtFindProizvod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFindProizvod_KeyPress);
            // 
            // proizvodiTableAdapter
            // 
            this.proizvodiTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Šifra";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Naziv";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(458, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Količina";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(540, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Cijena";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Broj računa:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(226, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Datum:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(407, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Ukupno:";
            // 
            // txtCijenaProizvoda
            // 
            this.txtCijenaProizvoda.Location = new System.Drawing.Point(523, 28);
            this.txtCijenaProizvoda.Name = "txtCijenaProizvoda";
            this.txtCijenaProizvoda.ReadOnly = true;
            this.txtCijenaProizvoda.Size = new System.Drawing.Size(84, 20);
            this.txtCijenaProizvoda.TabIndex = 9;
            this.txtCijenaProizvoda.TabStop = false;
            this.txtCijenaProizvoda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtKolicinaProizvoda
            // 
            this.txtKolicinaProizvoda.Location = new System.Drawing.Point(446, 28);
            this.txtKolicinaProizvoda.Name = "txtKolicinaProizvoda";
            this.txtKolicinaProizvoda.Size = new System.Drawing.Size(71, 20);
            this.txtKolicinaProizvoda.TabIndex = 10;
            this.txtKolicinaProizvoda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKolicinaProizvoda.TextChanged += new System.EventHandler(this.txtKolicinaProizvoda_TextChanged);
            this.txtKolicinaProizvoda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKolicinaProizvoda_KeyPress);
            // 
            // txtNazivProizvoda
            // 
            this.txtNazivProizvoda.Location = new System.Drawing.Point(120, 28);
            this.txtNazivProizvoda.Name = "txtNazivProizvoda";
            this.txtNazivProizvoda.ReadOnly = true;
            this.txtNazivProizvoda.Size = new System.Drawing.Size(320, 20);
            this.txtNazivProizvoda.TabIndex = 11;
            this.txtNazivProizvoda.TabStop = false;
            this.txtNazivProizvoda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBrRacuna
            // 
            this.txtBrRacuna.Location = new System.Drawing.Point(105, 19);
            this.txtBrRacuna.Name = "txtBrRacuna";
            this.txtBrRacuna.ReadOnly = true;
            this.txtBrRacuna.Size = new System.Drawing.Size(100, 20);
            this.txtBrRacuna.TabIndex = 12;
            this.txtBrRacuna.TabStop = false;
            this.txtBrRacuna.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDatumRacuna
            // 
            this.txtDatumRacuna.Location = new System.Drawing.Point(273, 19);
            this.txtDatumRacuna.Name = "txtDatumRacuna";
            this.txtDatumRacuna.ReadOnly = true;
            this.txtDatumRacuna.Size = new System.Drawing.Size(108, 20);
            this.txtDatumRacuna.TabIndex = 13;
            this.txtDatumRacuna.TabStop = false;
            this.txtDatumRacuna.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtUkupnoRacun
            // 
            this.txtUkupnoRacun.Location = new System.Drawing.Point(461, 19);
            this.txtUkupnoRacun.Name = "txtUkupnoRacun";
            this.txtUkupnoRacun.ReadOnly = true;
            this.txtUkupnoRacun.Size = new System.Drawing.Size(100, 20);
            this.txtUkupnoRacun.TabIndex = 14;
            this.txtUkupnoRacun.TabStop = false;
            this.txtUkupnoRacun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtCijenaProizvoda);
            this.groupBox1.Controls.Add(this.txtFindProizvod);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtNazivProizvoda);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtKolicinaProizvoda);
            this.groupBox1.Location = new System.Drawing.Point(295, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(613, 68);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtUkupnoRacun);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtDatumRacuna);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtBrRacuna);
            this.groupBox2.Location = new System.Drawing.Point(295, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(613, 53);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // fKRacuniStavkeProizvodiBindingSource
            // 
            this.fKRacuniStavkeProizvodiBindingSource.DataMember = "FK_RacuniStavke_Proizvodi";
            this.fKRacuniStavkeProizvodiBindingSource.DataSource = this.proizvodiBindingSource;
            // 
            // racuniStavkeTableAdapter
            // 
            this.racuniStavkeTableAdapter.ClearBeforeFill = true;
            // 
            // dgvStavke
            // 
            this.dgvStavke.AllowUserToAddRows = false;
            this.dgvStavke.AllowUserToDeleteRows = false;
            this.dgvStavke.AllowUserToResizeColumns = false;
            this.dgvStavke.AllowUserToResizeRows = false;
            this.dgvStavke.AutoGenerateColumns = false;
            this.dgvStavke.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStavke.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvStavke.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStavke.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.sifraDataGridViewTextBoxColumn1,
            this.idZaglavljaDataGridViewTextBoxColumn,
            this.nazivDataGridViewTextBoxColumn1,
            this.cijenaDataGridViewTextBoxColumn1,
            this.kolicinaDataGridViewTextBoxColumn,
            this.stavkaUkupno,
            this.idProizvoda});
            this.dgvStavke.DataSource = this.fKRacuniStavkeProizvodiBindingSource;
            this.dgvStavke.Location = new System.Drawing.Point(295, 137);
            this.dgvStavke.MultiSelect = false;
            this.dgvStavke.Name = "dgvStavke";
            this.dgvStavke.ReadOnly = true;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStavke.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dgvStavke.RowHeadersVisible = false;
            this.dgvStavke.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStavke.Size = new System.Drawing.Size(613, 385);
            this.dgvStavke.TabIndex = 16;
            this.dgvStavke.TabStop = false;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // sifraDataGridViewTextBoxColumn1
            // 
            this.sifraDataGridViewTextBoxColumn1.DataPropertyName = "sifra";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.sifraDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle16;
            this.sifraDataGridViewTextBoxColumn1.HeaderText = "sifra";
            this.sifraDataGridViewTextBoxColumn1.Name = "sifraDataGridViewTextBoxColumn1";
            this.sifraDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // idZaglavljaDataGridViewTextBoxColumn
            // 
            this.idZaglavljaDataGridViewTextBoxColumn.DataPropertyName = "idZaglavlja";
            this.idZaglavljaDataGridViewTextBoxColumn.HeaderText = "idZaglavlja";
            this.idZaglavljaDataGridViewTextBoxColumn.Name = "idZaglavljaDataGridViewTextBoxColumn";
            this.idZaglavljaDataGridViewTextBoxColumn.ReadOnly = true;
            this.idZaglavljaDataGridViewTextBoxColumn.Visible = false;
            this.idZaglavljaDataGridViewTextBoxColumn.Width = 20;
            // 
            // nazivDataGridViewTextBoxColumn1
            // 
            this.nazivDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nazivDataGridViewTextBoxColumn1.DataPropertyName = "naziv";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.nazivDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle17;
            this.nazivDataGridViewTextBoxColumn1.HeaderText = "naziv";
            this.nazivDataGridViewTextBoxColumn1.Name = "nazivDataGridViewTextBoxColumn1";
            this.nazivDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // cijenaDataGridViewTextBoxColumn1
            // 
            this.cijenaDataGridViewTextBoxColumn1.DataPropertyName = "cijena";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.cijenaDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle18;
            this.cijenaDataGridViewTextBoxColumn1.HeaderText = "cijena";
            this.cijenaDataGridViewTextBoxColumn1.Name = "cijenaDataGridViewTextBoxColumn1";
            this.cijenaDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // kolicinaDataGridViewTextBoxColumn
            // 
            this.kolicinaDataGridViewTextBoxColumn.DataPropertyName = "kolicina";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.kolicinaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle19;
            this.kolicinaDataGridViewTextBoxColumn.HeaderText = "kolicina";
            this.kolicinaDataGridViewTextBoxColumn.Name = "kolicinaDataGridViewTextBoxColumn";
            this.kolicinaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // stavkaUkupno
            // 
            this.stavkaUkupno.DataPropertyName = "stavkaUkupno";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.stavkaUkupno.DefaultCellStyle = dataGridViewCellStyle20;
            this.stavkaUkupno.HeaderText = "stavkaUkupno";
            this.stavkaUkupno.Name = "stavkaUkupno";
            this.stavkaUkupno.ReadOnly = true;
            // 
            // idProizvoda
            // 
            this.idProizvoda.DataPropertyName = "idProizvoda";
            this.idProizvoda.HeaderText = "idProizvoda";
            this.idProizvoda.Name = "idProizvoda";
            this.idProizvoda.ReadOnly = true;
            this.idProizvoda.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(833, 542);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Spremi";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(298, 542);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 18;
            this.btnDelete.Text = "Obriši";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblInfo
            // 
            this.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInfo.Location = new System.Drawing.Point(21, 542);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(268, 22);
            this.lblInfo.TabIndex = 19;
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KasaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 577);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgvStavke);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvProizvodi);
            this.Name = "KasaForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KasaForm";
            this.Load += new System.EventHandler(this.TestingKasaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProizvodi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proizvodiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pkrDataBaseDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fKRacuniStavkeProizvodiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStavke)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvProizvodi;
        private System.Windows.Forms.TextBox txtFindProizvod;
        private pkrDataBaseDataSet pkrDataBaseDataSet;
        private System.Windows.Forms.BindingSource proizvodiBindingSource;
        private pkrDataBaseDataSetTableAdapters.ProizvodiTableAdapter proizvodiTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCijenaProizvoda;
        private System.Windows.Forms.TextBox txtKolicinaProizvoda;
        private System.Windows.Forms.TextBox txtNazivProizvoda;
        private System.Windows.Forms.TextBox txtBrRacuna;
        private System.Windows.Forms.TextBox txtDatumRacuna;
        private System.Windows.Forms.TextBox txtUkupnoRacun;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.BindingSource fKRacuniStavkeProizvodiBindingSource;
        private pkrDataBaseDataSetTableAdapters.RacuniStavkeTableAdapter racuniStavkeTableAdapter;
        private System.Windows.Forms.DataGridView dgvStavke;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn sifraDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazivDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cijenaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProizvoidaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sifraDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idZaglavljaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazivDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cijenaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolicinaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stavkaUkupno;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProizvoda;
        private System.Windows.Forms.Label lblInfo;
    }
}