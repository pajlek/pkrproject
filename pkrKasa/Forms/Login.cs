﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using pkrClassLibrary;
using System.Data.SqlClient;

namespace pkrKasa.Forms
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        SqlConnection conn;
        SqlDataAdapter adpt;
        DataTable dt;
        

        private void Login_Load(object sender, EventArgs e)
        {
            conn = new SqlConnection(Properties.Settings.Default.pkrDataBaseConnectionString);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Return)
            {
                adpt = new SqlDataAdapter("SELECT * FROM Korisnici WHERE sifra ='" + txtSifra.Text + "'", conn);

                dt = new DataTable();
                adpt.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    AuthKorisnik.UlogiraniKorisnik = dt.Rows[0]["imeKorisnika"].ToString();
                    AuthKorisnik.RazinaPrava = (int)dt.Rows[0]["razinaPrava"];

                    Hide();
                    new MainForm().Show();
                }
                else
                {
                    MessageBox.Show("Kriva lozinka!");
                }
            }
            if (e.KeyChar == (char)Keys.Escape)
            {
                Application.Exit();
            }
        }
    }
}
