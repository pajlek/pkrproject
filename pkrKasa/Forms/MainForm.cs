﻿using pkrKasa.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using pkrClassLibrary;

namespace pkrKasa
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            lblUlogirani.Text = AuthKorisnik.UlogiraniKorisnik;
            if (AuthKorisnik.RazinaPrava != 0)
            {
                tsmiProizvodi.Available = false;
                tsmiKorisnici.Available = false;
            }
            else
            {
                tsmiProizvodi.Available = true;
                tsmiKorisnici.Available = true;
            }
        }

        private void proizvodiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            new ProizvodiForm().Show();
        }

        private void kasaTestingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            new KasaForm().Show();
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                Hide();
                new Login().Show();
            }
        }

        private void korisniciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            new KorisniciForm().Show();
        }
    }
}
